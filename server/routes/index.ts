import * as express from 'express';
import * as bodyParser from 'body-parser';
import routerCategory from './category';
import routerOption from './option';
import routerQuestion from './question';
import routerQuiz from './quiz';

const router = express.Router();
const jsonParser = bodyParser.json()

router.use(jsonParser); 

router.use('/api/', routerCategory);
router.use('/api/', routerOption);
router.use('/api/', routerQuestion);
router.use('/api/', routerQuiz);

export default router;


