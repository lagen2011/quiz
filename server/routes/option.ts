import OptionController from '../controllers/OptionController';
import Route from './CRUD';

const router = new Route(OptionController, 'options').getRoute();

export default router;


