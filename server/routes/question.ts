import QuestionController from '../controllers/QuestionController';
import Route from './CRUD';

const router = new Route(QuestionController, 'questions').getRoute();

export default router;


