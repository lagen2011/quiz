import Middleware from '../middelwares/apiWare';
import * as express from 'express';
import MainControllers from 'server/controllers/MainController';

class Route {
  router;
  constructor(controller: MainControllers, url : string) {
    this.router = express.Router();

    this.router.get(`/${url}`, controller.getAll);
    this.router.get(`/${url}/:id`, controller.getById);

    this.router.delete(`/${url}/:id`, controller.deleteById);

    this.router.use(Middleware.extendReqUser, Middleware.errorHandler);

    this.router.post(`/${url}`, controller.add);

    this.router.put(`/${url}/:id`, controller.editById);
  }
  getRoute() {
    return this.router;
  }
}

export default Route;


