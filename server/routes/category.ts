import CategoriesControllers from '../controllers/CategoryController';
import Route from './CRUD';

const router = new Route(CategoriesControllers, 'categories').getRoute();

router.get('/categories/title/:title', CategoriesControllers.getCategoryByTitle);

export default router;


