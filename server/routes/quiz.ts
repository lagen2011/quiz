import QuizController from '../controllers/QuizController';
import Route from './CRUD';

const router = new Route(QuizController, 'quizzes').getRoute();

export default router;