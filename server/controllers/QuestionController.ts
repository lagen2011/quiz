import question from '../models/Question';
import MainControllers from './MainController';

class QuestionControllers extends MainControllers {
    constructor() {
        super(question);
    }
};
export default new QuestionControllers();