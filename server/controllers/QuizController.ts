import quiz from '../models/Quiz';
import MainControllers from './MainController';

class QuizControllers extends MainControllers {
    constructor() {
        super(quiz);
    }
};
export default new QuizControllers();