import option from '../models/Option';
import MainControllers from './MainController';

class OptionControllers extends MainControllers {
    constructor() {
        super(option);
    }
};
export default new OptionControllers();