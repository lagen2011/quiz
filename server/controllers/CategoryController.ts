import categories from '../models/Categories';
import {
    Request,
    Response
} from 'express';
import MainControllers from './MainController';

class CategoryControllers extends MainControllers {
    constructor() {
        super(categories);
        this.getCategoryByTitle = this.getCategoryByTitle.bind(this);
    }
    async getCategoryByTitle(req: Request, res: Response) {
        const result = await categories.getByTitle(req.params.title);
        res.json(result);
    };
};
export default new CategoryControllers();