import categories from '../models/Categories';
import {
    Request,
    Response
} from 'express';
import { IDataRequest } from 'server/interfaces/IDataRequest';
import { ICRUDControllers } from 'server/interfaces/controllersDataBaseCRUD';
import { ICRUDModel } from 'server/interfaces/modelDataBaseCRUD';

class MainControllers implements ICRUDControllers {

    model: ICRUDModel

    constructor(model: ICRUDModel) {
        this.model = model;
        this.getAll = this.getAll.bind(this);
        this.getById = this.getById.bind(this);
        this.deleteById = this.deleteById.bind(this);
        this.editById = this.editById.bind(this);
        this.add = this.add.bind(this);
    }
    async getAll(req: Request, res: Response) {
        const result = await this.model.getAll();
        res.json(result);
    };
    async getById(req: Request, res: Response) {
        const result = await this.model.getById(req.params.id);
        res.json(result);
    };
    async deleteById(req: Request, res: Response) {
        const result = await this.model.deleteById(req.params.id);
        res.json(result);
    };
    async editById(req: Request & IDataRequest, res: Response) {
        const result = await this.model.editById(req.params.id, req.data);
        if (result) {
            res.end('True');
        }
        res.end('False');
    };
    async add(req: Request & IDataRequest, res: Response) {
        const result = await this.model.add(req.data);
        if (result) {
            res.end('True');
        }
        res.end('False');
    };
};
export default MainControllers;