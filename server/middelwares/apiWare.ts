import {
    Request,
    Response,
    NextFunction
} from 'express';
import { IDataRequest } from 'server/interfaces/IDataRequest';

export default {
    extendReqUser: (req: Request & IDataRequest, res: Response, next: NextFunction) => {
      if (req.body !== undefined) {
        req.data = req.body;
        next();
      } else {
        next('Error');
      }
    },
    errorHandler: (err: Error, req: Request, res: Response, next: NextFunction) => {
        next(err);
    }
};