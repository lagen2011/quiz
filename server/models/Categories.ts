import Queries from '../database/queries';
import MainFactory from './MainFactory';

class CategoryModel extends MainFactory {
    constructor() {
        super('categories');
    }
    getByTitle(title : string) {
        return Queries.getByProperty('categories', 'title', title);
    }
}
export default new CategoryModel();
