import MainFactory from './MainFactory';

class OptionModel extends MainFactory {
    constructor() {
        super('options');
    }
}
export default new OptionModel();