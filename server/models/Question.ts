import MainFactory from './MainFactory';

class QuestionModel extends MainFactory {
    constructor() {
        super('questions');
    }
}
export default new QuestionModel();