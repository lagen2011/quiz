import { ICategory } from '../interfaces/category';
import { ICRUDModel } from 'server/interfaces/modelDataBaseCRUD';
import Queries from '../database/queries';

class MainFactory implements ICRUDModel {

    private table : string;
    constructor(table : string) {
      this.table = table;
    }
    getAll() {
        return Queries.getAll(this.table);
    }
    getById(id : string) {
        return Queries.getByProperty(this.table, 'id', id);
    }
    deleteById(id : string) {
        return Queries.deleteByProperty(this.table, 'id', id);
    }
    add(category : ICategory) {
        return Queries.addValue(this.table, category);
    }
    editById(id : string, category : ICategory) {
        return Queries.editById(this.table, id,  category);
    }
}
export default MainFactory;
