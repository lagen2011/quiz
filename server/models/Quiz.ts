import MainFactory from './MainFactory';

class QuizModel extends MainFactory {
    constructor() {
        super('quizzes');
    }
}
export default new QuizModel();