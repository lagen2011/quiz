export interface ICRUDModel {
  getAll() : Promise<any>;
  getById(id : string) : Promise<any>;
  add(data : any) : Promise<boolean>;
  editById(id : string, data : any) : Promise<boolean>;
  deleteById(id : string) : Promise<boolean>;
}