export interface IQuize {
  id : string ;
  title : string;
  description: string;
}